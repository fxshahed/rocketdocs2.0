import { HomePage } from "../page-objects/HomePage";
import credentital from "../page-objects/credentital";
import ControlCenter from "../page-objects/PaneObject/controlCenter";
import oktacredentital from "../page-objects/oktaLoginPage";
import 'cypress-file-upload';

describe("Contant Management Enhancement", () => {
  const homePage = new HomePage();
  const controlCenter = new ControlCenter();

  before(() => {
    if (Boolean = true) {
      cy.visit("/")
      cy.visit(homePage.navigateToRocketdocsPage())
      credentital.Email
      credentital.Password
      credentital.SignIn
    } else {
      // Okta Credentital Login page 
      cy.visit("/")
      cy.visit(homePage.navigateToRocketdocsPage())
      oktacredentital.LoginOrganization
      oktacredentital.UserName
      oktacredentital.Password
      oktacredentital.SignIn
    }
  });

  it('RD20-1801: Verify Content Deletion From Search Reasult', () => {
    expect(cy)
      .property('xpath')
      .to.be.a('function')
    // Expand Libraries to select one for adding new content within it 
    cy.wait(5000)
    cy.get(controlCenter.navigateControlCenterOptions).contains('Libraries', { timeout: 3000 }).click({ force: true }).then(() => {
      cy.get(controlCenter.navigateControlCenterOptions).contains('shahed test', { timeout: 3000 }).click({ force: true }).then(() => {
        // Stage Summary
        cy.get('img[alt="Add"]', { timeout: 3000 }).click().then(() => {
          // Select question/topics
          cy.get('input[name=topic').type('Deletion Content Test')
          // Select Content type 
          cy.get('#mui-component-select-contentType').click({ force: true })
          cy.xpath('//body/div[@id="menu-contentType"]').contains('Shahed T1').click({ force: true })
          //Selecting Library
          cy.get('#mui-component-select-library', { timeout: 3000 }).click({ force: true })
          cy.get('.Mui-selected').contains('shahed test').click()
          // Selecting SME
          cy.get('#mui-component-select-sme', { timeout: 5000 }).click({ force: true })
          cy.xpath('//body/div[@id="menu-sme"]', { timeout: 3000 }).contains('Ali, MDShahed').click({ force: true })
          // Selecting review Cycle
          cy.get('#mui-component-select-reviewCycle').click({ force: true })
          cy.contains('Bi-annual').click()
          // Selecting Attribute Profile
          cy.get('#mui-component-select-attributeProfile').click({ force: true })
          cy.xpath('//body/div[@id="menu-attributeProfile"]').contains('ABC').click()
          cy.get('.MuiBox-root > form').contains('Next').click({ force: true })
          // Classification
          cy.get('.MuiBox-root > form').contains('Next').click({ force: true })
          // Assignment Screen
          cy.get('.MuiBox-root > form').contains('Next').click({ force: true })
          // Response phase
          cy.get('textarea[name=text]').clear().type('Deletion enhancement Test')
          // File Upload for test Content 
          const fixtureFile = 'RapidDocs US Letter.docx';
          cy.get('input[name="file"]').attachFile(fixtureFile);
          // Submition New Content to Lib
          cy.get('.MuiBox-root > form', { timeout: 10000 }).contains('Submit').click()
          // Navigate Back to main Screen & perform Seach for New Added Content 
          cy.get('div[class="MuiNav-logoDiv"]').click({ timeout: 5000 })
          cy.wait(10000)
          cy.get('input[placeholder="Search"]', { timeout: 5000 }).clear().type('Deletion Content Test').type('{enter}')
          cy.wait(20000)
          // Verify New Content is visible & Perform Deletion
          cy.reload()
          cy.xpath('//td[contains(text(),"Deletion Content Test")]').then($td => {
            if ($td.is(':visible')) {
            }
          })
          cy.xpath('//tbody/tr[1]/td[1]/span[1]/span[1]/input[1]').check()
          cy.get('[alt="Delete"]').click()
          cy.xpath('//div[contains(text(),"Are you sure you want to delete the selected content?")]')
            .should('have.text', 'Are you sure you want to delete the selected content? They cannot be retrieved once they have been deleted. This operation will remove links to any other entities');
          cy.get('button[type=button]').contains('Yes').click({ force: true })

          // Navigate back to Lib & Re-verify the deleted content
          cy.get('div[class="MuiNav-logoDiv"]').click({ timeout: 5000 })
          cy.wait(5000)
          cy.get(controlCenter.navigateControlCenterOptions).contains('Libraries', { timeout: 3000 }).click({ force: true }).then(() => {
            cy.get(controlCenter.navigateControlCenterOptions).contains('shahed test', { timeout: 3000 }).click({ force: true }).then(() => {
              // Verify New content no longer available on Libraries after perform deletion on Search Result Screen
              cy.reload()
              cy.xpath('//i[contains(text(),"No Records Found.")]').then($td => {
                if ($td.is(':visible')) {
                }
              })
            })
          })
        })
      })
    })
  })
})
