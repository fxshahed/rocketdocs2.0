import { HomePage } from "../page-objects/HomePage";
import credentital from "../page-objects/credentital";
import ControlCenter from "../page-objects/PaneObject/controlCenter";
import oktacredentital from "../page-objects/oktaLoginPage";
import 'cypress-file-upload';

describe("Content Attribute", () => {
    const homePage = new HomePage();
    const controlCenter = new ControlCenter();

    before(() => {
        if (Boolean = true) {
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            credentital.Email
            credentital.Password
            credentital.SignIn
        } else {
            // Okta Credentital Login page 
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            oktacredentital.LoginOrganization
            oktacredentital.UserName
            oktacredentital.Password
            oktacredentital.SignIn
        }
    });

    it('RD20-860,861,862: Verify Add, Modify and Delete Content Attribute', () => {
        expect(cy)
            .property('xpath')
            .to.be.a('function')
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false;
        });
        /*RD20-860 Add Content Attribute*/
        cy.wait(5000)
        cy.get(controlCenter.navigateControlCenterOptions).contains('Administration', { timeout: 3000 }).click({ force: true }).then(() => {
            cy.get('img[alt="Settings"]').click({ force: true }).then(() => {
                cy.wait(5000)
                cy.xpath('//span[contains(text(),"Content Settings")]').click().then(() => {
                    cy.wait(5000)
                    cy.get('.MuiGrid-grid-sm-7 > .MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label').click().then(() => {
                        cy.wait(5000)
                        cy.get('input[name="name"]').clear().type('Add Attribute Test')
                        cy.get('input[name="showInList"]').click()
                        cy.get('input[name="searchFacet"]').click()
                        cy.get('input[name="includeInExport"]').click()
                        cy.get('div[id="mui-component-select-type"]').click().then(() => {
                            cy.xpath('//body/div[@id="menu-type"]').contains('Contact').click().then(() => {
                                cy.get('div[id="mui-component-select-groupId"]').click().then(() => {
                                    cy.xpath('//body/div[@id="menu-groupId"]').contains('Admin').click()
                                })
                            })
                        })
                    })
                })
                cy.get('.MuiBox-root > form').contains('Save').click()
                cy.wait(5000)
                cy.reload()
                /*RD20-861 Modify Content Attribute*/
                cy.xpath('//p[contains(text(),"Add Attribute Test")]').contains('Add Attribute Test').click().then(() => {
                    cy.wait(5000)
                    cy.get('input[name="name"]').clear().type('Add Attribute Test Execute')
                    cy.get('div[id="mui-component-select-groupId"]').click().then(() => {
                        cy.xpath('//body/div[@id="menu-groupId"]').contains('QA').click()
                    })
                })
                cy.get('.MuiBox-root > form').contains('Save').click()
                cy.wait(5000)
                cy.reload()
                /*RD20-862 Delete Content Attribute*/
                cy.xpath('//p[contains(text(),"Add Attribute Test Execute")]').contains('Add Attribute Test Execute').click().then(() => {
                    cy.wait(5000)
                    cy.get('button[type="button"]').contains('Delete Attribute').click().then(() => {
                        cy.get(':nth-child(19) > .MuiDialog-container > .MuiPaper-root > .MuiDialogActions-root').contains('Yes').dblclick()
                    })
                })
                cy.wait(5000)
                cy.reload()
                /*Verify New Content Attribute  no longer exist on Content Attribute*/
                cy.contains('Add Attribute Test Execute').should('not.exist');
            })
        })
    })
})

