import { HomePage } from "../page-objects/HomePage";
import credentital from "../page-objects/credentital";
import ControlCenter from "../page-objects/PaneObject/controlCenter";
import oktacredentital from "../page-objects/oktaLoginPage";
import 'cypress-file-upload';

describe("Content library", () => {
    const homePage = new HomePage();
    const controlCenter = new ControlCenter();

    before(() => {
        if (Boolean = true) {
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            credentital.Email
            credentital.Password
            credentital.SignIn
        } else {
            // Okta Credentital Login page 
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            oktacredentital.LoginOrganization
            oktacredentital.UserName
            oktacredentital.Password
            oktacredentital.SignIn
        }
    });

    it('RD20-102,266,267: Verify Add, Modify and Delete Content library', () => {
        expect(cy)
            .property('xpath')
            .to.be.a('function')
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false;
        });
        /*RD20-947 Add Content library*/
        cy.wait(5000)
        cy.get(controlCenter.navigateControlCenterOptions).contains('Administration', { timeout: 3000 }).click({ force: true }).then(() => {
            cy.get('img[alt="Settings"]').click({ force: true }).then(() => {
                cy.wait(5000)
                cy.xpath('//span[contains(text(),"Content Settings")]').click().then(() => {
                    cy.wait(5000)
                    cy.get(':nth-child(1) > .MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label').click().then(() => {
                        cy.wait(5000)
                        cy.get('input[name="name"]').clear().type('Content Library Test')
                        /*cy.get('#mui-component-select-defaultContentType').click().then(() => {
                            cy.xpath('//body/div[@id="menu-language"]').contains('English').click()
                        })*/
                        cy.get('div[id="mui-component-select-defaultContentType"]').click().then(() => {
                            cy.xpath('//body/div[@id="menu-defaultContentType"]').contains('Default').click()
                        })
                        cy.get('div[id="mui-component-select-groups"]').click().then(() => {
                            cy.xpath('//body/div[@id="menu-groups"]').contains('Admin').click({ force: true })
                        })
                        cy.get('.MuiBox-root > form').contains('Save').click({ force: true })
                    })
                })
                cy.wait(5000)
                cy.reload()
                /*RD20-948 Modify Content library*/
                cy.xpath('//p[contains(text(),"Content Library Test")]').contains('Content Library Test').click().then(() => {
                    cy.wait(5000)
                    cy.get('input[name="name"]').clear().type('Content Library Test1')
                    cy.get('div[id="mui-component-select-defaultContentType"]').click().then(() => {
                        cy.xpath('//body/div[@id="menu-defaultContentType"]').contains('Shahed T1').click()
                    })
                    cy.get('div[id="mui-component-select-groups"]').click().then(() => {
                        cy.xpath('//body/div[@id="menu-groups"]').contains('Admin').click()
                        cy.xpath('//body/div[@id="menu-groups"]').contains('QA').click()
                    })
                    cy.get('.MuiBox-root > form').contains('Save').click({ force: true })
                })
                cy.wait(5000)
                cy.reload()
                /*RD20-949 Delete Content library*/
                cy.xpath('//p[contains(text(),"Content Library Test1")]').contains('Content Library Test1').click().then(() => {
                    cy.wait(5000)
                    cy.get('.MuiBox-root > form').contains('Delete Library').click().then(() => {
                        cy.get(':nth-child(17) > .MuiDialog-container > .MuiPaper-root > .MuiDialogActions-root > .jss45').dblclick()

                    })
                })
                cy.wait(5000)
                cy.reload()
                /*Verify New Project Types no longer exist on Project Types*/
                cy.contains('Content Library Test1")]').should('not.exist');
            })
        })
    })
})

