import { HomePage } from "../page-objects/HomePage";
import credentital from "../page-objects/credentital";
import ControlCenter from "../page-objects/PaneObject/controlCenter";
import oktacredentital from "../page-objects/oktaLoginPage";
import 'cypress-file-upload';

describe("Content Attribute Profile", () => {
    const homePage = new HomePage();
    const controlCenter = new ControlCenter();

    before(() => {
        if (Boolean = true) {
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            credentital.Email
            credentital.Password
            credentital.SignIn
        } else {
            // Okta Credentital Login page 
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            oktacredentital.LoginOrganization
            oktacredentital.UserName
            oktacredentital.Password
            oktacredentital.SignIn
        }
    });

    it('RD20-863,864,865: Verify Add, Modify and Delete Content Attribute Profile', () => {
        expect(cy)
            .property('xpath')
            .to.be.a('function')
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false;
        });
        /*RD20-863 Add Content Attribute Profile*/
        cy.wait(5000)
        cy.get(controlCenter.navigateControlCenterOptions).contains('Administration', { timeout: 3000 }).click({ force: true }).then(() => {
            cy.get('img[alt="Settings"]').click({ force: true }).then(() => {
                cy.wait(5000)
                cy.xpath('//span[contains(text(),"Content Settings")]').click().then(() => {
                    cy.wait(5000)
                    cy.get('.MuiGrid-grid-sm-5 > .MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label').click().then(() => {
                        cy.wait(5000)
                        cy.get('input[name="name"]').clear().type('Add Attribute Profile test')
                        cy.get('.MuiFormControl-root-459').clear().type('10')
                        cy.get('.MuiFormControl-root-502').click().then(() => {
                            cy.xpath('//body/div[@id="menu-attributes.1.value"]').contains('Ali, MDShahed').click()
                        })
                    })
                })
                cy.get('.MuiBox-root > form').contains('Save').click()
                cy.wait(5000)
                cy.reload()
                /*RD20-864 Modify Content Attribute Profile*/
                cy.xpath('//p[contains(text(),"Add Attribute Profile test")]').contains('Add Attribute Profile test').click().then(() => {
                    cy.wait(5000)
                    cy.get('input[name="name"]').clear().type('Add Attribute Profile test1')
                    cy.get('.MuiFormControl-root-184').clear().type('5')
                    cy.get('.MuiFormControl-root-227').click().then(() => {
                            cy.xpath('//body/div[@id="menu-attributes.1.value"]').contains('Cindy, Zhang').click()
                        })
                })
                cy.get('.MuiBox-root > form').contains('Save').click()
                cy.wait(5000)
                cy.reload()
                /*RD20-865 Delete Content Attribute Profile*/
                cy.xpath('//p[contains(text(),"Add Attribute Profile test1")]').contains('Add Attribute Profile test1').click().then(() => {
                    cy.wait(5000)
                    cy.get('button[type="button"]').contains('Delete Attribute Profile').click().then(() => {
                        cy.get(':nth-child(20) > .MuiDialog-container > .MuiPaper-root > .MuiDialogActions-root').contains('Yes').dblclick({timeout: 2000})
                    })
                })
                cy.wait(5000)
                cy.reload()
                /*Verify New Content Attribute Profile no longer exist on Content Attribute Profile*/
                cy.contains('Add Attribute Profile test1').should('not.exist');
            })
        })
    })
})

