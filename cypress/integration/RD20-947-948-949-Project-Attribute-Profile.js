import { HomePage } from "../page-objects/HomePage";
import credentital from "../page-objects/credentital";
import ControlCenter from "../page-objects/PaneObject/controlCenter";
import oktacredentital from "../page-objects/oktaLoginPage";
import 'cypress-file-upload';

describe("Project Attributes Profile", () => {
    const homePage = new HomePage();
    const controlCenter = new ControlCenter();

    before(() => {
        if (Boolean = true) {
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            credentital.Email
            credentital.Password
            credentital.SignIn
        } else {
            // Okta Credentital Login page 
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            oktacredentital.LoginOrganization
            oktacredentital.UserName
            oktacredentital.Password
            oktacredentital.SignIn
        }
    });

    it('RD20-947,948,949: Verify Add, Modify and Delete Project Attribute Profile', () => {
        expect(cy)
            .property('xpath')
            .to.be.a('function')
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false;
        });
        /*RD20-947 Add Project Attribute Profile*/
        cy.wait(5000)
        cy.get(controlCenter.navigateControlCenterOptions).contains('Administration', { timeout: 3000 }).click({ force: true }).then(() => {
            cy.get('img[alt="Settings"]').click({ force: true }).then(() => {
                cy.get('img[alt="Project Settings"]').click({ timeout: 3000 }).then(() => {
                    cy.wait(5000)
                    cy.get('.MuiGrid-grid-sm-5 > .MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label').click().then(() => {
                        cy.get('input[name="name"]').clear().type('Test QA')
                        cy.get('input[name="attributes.0.value"]').clear().type('Attribute Profile Test')
                        cy.xpath('//div[@id="mui-component-select-attributes.1.value"]').click().then(() => {
                            cy.get('li[data-value="qa,dev,prod"]').click()
                            cy.get('li[data-value="leadership,CSM,devteam"]').click()
                        })
                        cy.get('div[id="mui-component-select-attributes.2.value"]').click({force: true}).then(() => {
                            cy.xpath('//body/div[@id="menu-attributes.2.value"]').contains('Ali, MDShahed').click()
                        })
                        cy.get('button[type="submit"]').contains('Save').click({force: true})
                    })
                })
                cy.reload()
                cy.wait(5000)
                /*RD20-948 Modify Project Attribute Profile*/
                cy.xpath('//p[contains(text(),"Test QA")]').click().then(() => {
                    cy.get('input[name="name"]').clear().type('Test QA1')
                        cy.get('input[name="attributes.0.value"]').clear().type('Attribute Profile Test')
                        cy.xpath('//div[@id="mui-component-select-attributes.1.value"]').click().then(() => {
                            cy.get('li[data-value="leadership,CSM,devteam"]').click()
                            cy.get('li[data-value="qa,dev,prod"]').click()
                        })
                        cy.get('div[id="mui-component-select-attributes.2.value"]').click({force: true}).then(() => {
                            cy.xpath('//body/div[@id="menu-attributes.2.value"]').contains('Ali, MDShahed').click()
                        })
                        cy.get('button[type="submit"]').contains('Save').click({force: true})
                })
                cy.reload()
                cy.wait(5000)
                /*RD20-949 Delete Project Attribute Profile*/
                cy.xpath('//p[contains(text(),"Test QA1")]').click().then(() => {
                        cy.get('button[type="button"]').contains('Delete Attribute Profile').click().then(() => {
                            cy.wait(3000)
                            cy.get(':nth-child(21) > .MuiDialog-container > .MuiPaper-root > .MuiDialogActions-root > .jss46').trigger('click')
                            cy.wait(3000)
                        })
                })
                cy.wait(5000)
                /*Verify New Attribute no longer exist on Project Attribute Profile*/
                cy.contains('Test QA1').should('not.exist');
            })
        })
    })
})
