import { HomePage } from "../page-objects/HomePage";
import LoginPage from "../page-objects/LoginPage";
import OktaLoginPage from "../page-objects/oktaLoginPage";

describe("Rocketdocs Login", () => {
    const homePage = new HomePage();
    const loginPage = new LoginPage();
    const oktaLogin = new OktaLoginPage();

    it('RD20-68: Verify RocketDocs reguler login', () => {
        if (Boolean = true) {
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            cy.get(loginPage.emailAddersInput()).type('mshahed@rocketdocs.com');
            cy.get(loginPage.passwordInput()).type('FXsr1987*');
            cy.get(loginPage.signInbutton()).click();
            cy.wait(2500)
        } else {
            // Okta Credentital Login page 
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            cy.get(oktaLogin.loginYourOrganization()).contains('Login through your organization').click();
            cy.get(oktaLogin.userNameInput()).type('mshahed@rocketdocs.com');
            cy.get(oktaLogin.passwordInput()).type('FXsr1987*');
            cy.get(oktaLogin.signInbutton()).click();
            cy.wait(2500)
        }
    })
})
