import { HomePage } from "../page-objects/HomePage";
import credentital from "../page-objects/credentital";
import ControlCenter from "../page-objects/PaneObject/controlCenter";
import oktacredentital from "../page-objects/oktaLoginPage";
import 'cypress-file-upload';

describe("Project Types", () => {
    const homePage = new HomePage();
    const controlCenter = new ControlCenter();

    before(() => {
        if (Boolean = true) {
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            credentital.Email
            credentital.Password
            credentital.SignIn
        } else {
            // Okta Credentital Login page 
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            oktacredentital.LoginOrganization
            oktacredentital.UserName
            oktacredentital.Password
            oktacredentital.SignIn
        }
    });

    it('RD20-135,268,269: Verify Add, Modify and Delete Project Types', () => {
        expect(cy)
            .property('xpath')
            .to.be.a('function')
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false;
        });
        /*RD20-947 Add Project Types*/
        cy.wait(5000)
        cy.get(controlCenter.navigateControlCenterOptions).contains('Administration', { timeout: 3000 }).click({ force: true }).then(() => {
            cy.get('img[alt="Settings"]').click({ force: true }).then(() => {
                cy.get('img[alt="Project Settings"]').click({ timeout: 3000 }).then(() => {
                    cy.wait(5000)
                    cy.get(':nth-child(1) > .MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label').click()
                    cy.wait(5000)
                    cy.get('input[name="name"]').clear().type('Project Types Test')
                    cy.get('div[id="mui-component-select-reviewCycle"]').click().then(() => {
                        cy.get('div[id="menu-reviewCycle"]').contains('Annual').click()
                    })
                    cy.get('img[alt="Delete"]').click().then(() => {
                        cy.get('.MuiGrid-justify-xs-space-between > .MuiButtonBase-root > .MuiIconButton-label').click()
                        cy.get('input[name="workflowStates.0.name"]').clear().type('Initiate')
                        cy.xpath('//div[@id="mui-component-select-workflowStates.0.group"]').click().then(() => {
                            cy.xpath('//body/div[@id="menu-workflowStates.0.group"]').contains('Admin').click()
                        })
                    })
                    cy.get('.MuiGrid-justify-xs-space-between > .MuiButtonBase-root > .MuiIconButton-label').click()
                    cy.get('input[name="workflowStates.1.name"]').clear().type('Freeze')
                    cy.xpath('//div[@id="mui-component-select-workflowStates.1.group"]').click().then(() => {
                        cy.xpath('//body/div[@id="menu-workflowStates.1.group"]').contains('Admin').click()
                    })
                    cy.get('.MuiGrid-justify-xs-space-between > .MuiButtonBase-root > .MuiIconButton-label').click()
                    cy.get('input[name="workflowStates.2.name"]').clear().type('Review')
                    cy.xpath('//div[@id="mui-component-select-workflowStates.2.group"]').click().then(() => {
                        cy.xpath('//body/div[@id="menu-workflowStates.2.group"]').contains('Admin').click()
                    })
                    cy.get('.MuiGrid-justify-xs-space-between > .MuiButtonBase-root > .MuiIconButton-label').click()
                    cy.get('input[name="workflowStates.3.name"]').clear().type('Approval')
                    cy.xpath('//div[@id="mui-component-select-workflowStates.3.group"]').click().then(() => {
                        cy.xpath('//body/div[@id="menu-workflowStates.3.group"]').contains('Admin').click()
                    })
                    cy.get('.MuiGrid-justify-xs-space-between > .MuiButtonBase-root > .MuiIconButton-label').click()
                    cy.get('input[name="workflowStates.4.name"]').clear().type('Approved')
                    cy.xpath('//div[@id="mui-component-select-workflowStates.4.group"]').click().then(() => {
                        cy.xpath('//body/div[@id="menu-workflowStates.4.group"]').contains('Admin').click()
                    })
                    cy.get('button[type="submit"]').click()
                })
                cy.wait(5000)
                cy.reload()
                /*RD20-948 Modify Project Types*/
                cy.xpath('//p[contains(text(),"Project Types Test")]').contains('Project Types Test').click().then(() => {
                    cy.get('input[name="name"]').clear().type('Project Types Test Execute')
                    cy.get('div[id="mui-component-select-reviewCycle"]').click().then(() => {
                        cy.get('div[id="menu-reviewCycle"]').contains('Annual').click()
                    })
                    cy.get('.Mui-selected').contains('Initiate').click()
                    cy.get('input[name="workflowStates.0.name"]').clear().type('InitiateDoc')
                    cy.xpath('//div[@id="mui-component-select-workflowStates.0.group"]').click().then(() => {
                        cy.xpath('//body/div[@id="menu-workflowStates.0.group"]').contains('QA').click()
                    })
                    cy.get('button[type="submit"]').click()
                    cy.wait(5000)
                    cy.xpath('//p[contains(text(),"Project Types Test Execute")]').contains('Project Types Test Execute').click().then(() => {
                        cy.wait(5000)
                        cy.get('[data-rbd-draggable-id="step-1"]').contains('Freeze').click()
                        cy.get('input[name="workflowStates.1.name"]').clear().type('FreezeDoc')
                        cy.xpath('//div[@id="mui-component-select-workflowStates.1.group"]').click().then(() => {
                            cy.xpath('//body/div[@id="menu-workflowStates.1.group"]').contains('QA').click()
                        })
                        cy.get('button[type="submit"]').click()
                    })
                    cy.wait(5000)
                    cy.xpath('//p[contains(text(),"Project Types Test Execute")]').contains('Project Types Test Execute').click().then(() => {
                        cy.wait(5000)
                        cy.get('[data-rbd-draggable-id="step-2"]').contains('Review').click()
                        cy.get('input[name="workflowStates.2.name"]').clear().type('ReviewDoc')
                        cy.xpath('//div[@id="mui-component-select-workflowStates.2.group"]').click().then(() => {
                            cy.xpath('//body/div[@id="menu-workflowStates.2.group"]').contains('QA').click()
                        })
                        cy.get('button[type="submit"]').click()
                    })
                    cy.wait(5000)
                    cy.xpath('//p[contains(text(),"Project Types Test Execute")]').contains('Project Types Test Execute').click().then(() => {
                        cy.wait(5000)
                        cy.get('[data-rbd-draggable-id="step-3"]').contains('Approval').click()
                        cy.get('input[name="workflowStates.3.name"]').clear().type('ApprovalDoc')
                        cy.xpath('//div[@id="mui-component-select-workflowStates.3.group"]').click().then(() => {
                            cy.xpath('//body/div[@id="menu-workflowStates.3.group"]').contains('QA').click()
                        })
                        cy.get('button[type="submit"]').click()
                    })
                    cy.wait(5000)
                    cy.xpath('//p[contains(text(),"Project Types Test Execute")]').contains('Project Types Test Execute').click().then(() => {
                        cy.wait(5000)
                        cy.get('[data-rbd-draggable-id="step-4"]').contains('Approved').click()
                        cy.get('input[name="workflowStates.4.name"]').clear().type('ApprovedDoc')
                        cy.xpath('//div[@id="mui-component-select-workflowStates.4.group"]').click().then(() => {
                            cy.xpath('//body/div[@id="menu-workflowStates.4.group"]').contains('QA').click()
                        })
                        cy.get('button[type="submit"]').click()
                    })
                    cy.wait(5000)
                })
                cy.reload()
                /*RD20-949 Delete Project Types*/
                cy.xpath('//p[contains(text(),"Project Types Test Execute")]').contains('Project Types Test Execute').click().then(() => {
                    cy.get('button[type="button"]').contains('Delete Project Type').click().then(() => {
                        cy.wait(5000)
                        cy.get(':nth-child(17) > .MuiDialog-container > .MuiPaper-root > .MuiDialogActions-root').contains('Yes').dblclick()
                    })
                })
                cy.wait(5000)
                /*Verify New Project Types no longer exist on Project Types*/
                cy.contains('Project Types Test Execute').should('not.exist');
            })
        })
    })
})

