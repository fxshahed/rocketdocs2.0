import { HomePage } from "../page-objects/HomePage";
import credentital from "../page-objects/credentital";
import ControlCenter from "../page-objects/PaneObject/controlCenter";
import oktacredentital from "../page-objects/oktaLoginPage";
import 'cypress-file-upload';
import Projects from "../page-objects/Project/projectsObject";

describe("Create Project", () => {
    const homePage = new HomePage();
    const controlCenter = new ControlCenter();
    const projects = new Projects()

    before(() => {
        if (Boolean = true) {
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            credentital.Email
            credentital.Password
            credentital.SignIn
        } else {
            // Okta Credentital Login page 
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            oktacredentital.LoginOrganization
            oktacredentital.UserName
            oktacredentital.Password
            oktacredentital.SignIn
        }
    });

    it('RD20-151: Verify Create Project', () => {
        expect(cy)
            .property('xpath')
            .to.be.a('function')
        // Navigate Project to add new project 
        cy.wait(5000)
        cy.get(controlCenter.navigateControlCenterOptions).contains('Projects', { timeout: 3000 }).click({ force: true }).then(() => {
            // Verify [+] icon button is visible
            cy.get('img[alt="Add"]').then($button => {
                if ($button.is(':visible')) {
                }
            })
            cy.get('img[alt="Add"]', { timeout: 3000 }).click().then(() => {
                // Start Fill-out About project & start with input title name
                cy.wait(5000)
                cy.get('input[name="title"]').clear().type('Shahed TT1')
                // Slect project type
                cy.get('div[id="mui-component-select-projectType"]').click({ force: true })
                cy.xpath('//body/div[@id="menu-projectType"]').contains('mshahed Test').click({ force: true })
                // Enter Value for client 
                cy.get('input[name="client"]').clear().type('QA')
                // Select language 
                cy.get('#mui-component-select-language').click()
                cy.xpath('//body/div[@id="menu-language"]').contains('English').click()
                // Select Attribute Profile
                cy.get('div[id="mui-component-select-attributeProfile"]').click()
                cy.xpath('//body/div[@id="menu-attributeProfile"]').contains('ABC').click({ force: true })
                // Select Review cycle
                cy.get('div[id="mui-component-select-reviewCycle"]').click()
                cy.xpath('//body/div[@id="menu-reviewCycle"]').contains('Annual').click({ force: true })
                cy.get('button[type="submit"]').click()
                cy.get('button[type="submit"]').click()
                // Add project Document
                const fixtureFile = 'RFP Vendor RFI.xlsx';
                cy.get('input[name="file"]').attachFile(fixtureFile);
                cy.get('button[type="submit"]').click()
                // Select project lead 
                cy.get('div[id="mui-component-select-assignedTo"]').click()
                cy.xpath('//body/div[@id="menu-assignedTo"]').contains('Ali, MDShahed').click()
                // Input note 
                cy.get('textarea[name="note"]').clear().type('Test Note')
                // Submit Project
                cy.get('button[type="submit"]').click()
                cy.wait(5000)
                // Navigation to new project 
                cy.get(projects.navigateProjectScreen).contains('Shahed TT1', { timeout: 3000 }).click({ force: true }).then(() => {
                    cy.wait(5000)
                    cy.get('div[class="MuiNav-content"]').contains('Back').click({ force: true })
                    cy.wait(5000)
                  })
                // Removed New project
                cy.xpath('//thead/tr[1]/th[1]').click().then(() => {
                    cy.get('.MuiButton-containedSizeSmall').click()
                    cy.xpath('//ul[@id="split-button-menu"]').contains('Archive').click({ force: true })
                })
                // Verify project is no longer exist
                cy.reload()
                cy.get('.MuiTable-root').should('not.exist', 'Shahed TT1')
            })
        })
    })
})
