import { HomePage } from "../page-objects/HomePage";
import credentital from "../page-objects/credentital";
import ControlCenter from "../page-objects/PaneObject/controlCenter";
import oktacredentital from "../page-objects/oktaLoginPage";
import 'cypress-file-upload';

describe("Content Placeholder", () => {
    const homePage = new HomePage();
    const controlCenter = new ControlCenter();

    before(() => {
        if (Boolean = true) {
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            credentital.Email
            credentital.Password
            credentital.SignIn
        } else {
            // Okta Credentital Login page 
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            oktacredentital.LoginOrganization
            oktacredentital.UserName
            oktacredentital.Password
            oktacredentital.SignIn
        }
    });

    it('RD20-866,867,868: Verify Add, Modify and Delete Content Placeholder', () => {
        expect(cy)
            .property('xpath')
            .to.be.a('function')
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false;
        });
        /*RD20-866 Add Content Placeholder*/
        cy.wait(5000)
        cy.get(controlCenter.navigateControlCenterOptions).contains('Administration', { timeout: 3000 }).click({ force: true }).then(() => {
            cy.get('img[alt="Settings"]').click({ force: true }).then(() => {
                cy.wait(5000)
                cy.xpath('//span[contains(text(),"Content Settings")]').click().then(() => {
                    cy.wait(5000)
                    cy.get(':nth-child(5) > .MuiToolbar-root > :nth-child(2) > .MuiIconButton-label').click().then(() => {
                        cy.wait(5000)
                        cy.get('input[name="name"]').clear().type('Add Content Placeholder Test')
                        cy.get('input[name="value"]').clear().type('rocketdocs2.0')
                    })
                })
                cy.get('.MuiBox-root > form').contains('Save').click()
                cy.wait(5000)
                cy.reload()
                /*RD20-867 Modify Content Placeholder*/
                cy.xpath('//p[contains(text(),"Add Content Placeholder Test")]').contains('Add Content Placeholder Test').click().then(() => {
                    cy.wait(5000)
                    cy.get('input[name="name"]').clear().type('Add Content Placeholder Test1')
                    cy.get('input[name="value"]').clear().type('rocketdocs2.0.0')
                })
                cy.get('.MuiBox-root > form').contains('Save').click()
                cy.wait(5000)
                cy.reload()
                /*RD20-868 Delete Content Placeholder*/
                cy.xpath('//p[contains(text(),"Add Content Placeholder Test1")]').contains('Add Content Placeholder Test1').click().then(() => {
                    cy.wait(5000)
                    cy.get('button[type="button"]').contains('Delete Placeholder').click()
                })
                cy.wait(5000)
                cy.reload()
                /*Verify New Content Placeholder no longer exist on Content Placeholder*/
                cy.contains('Add Content Placeholder Test1').should('not.exist');
            })
        })
    })
})

