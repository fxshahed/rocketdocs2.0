import { HomePage } from "../page-objects/HomePage";
import credentital from "../page-objects/credentital";
import ControlCenter from "../page-objects/PaneObject/controlCenter";
import oktacredentital from "../page-objects/oktaLoginPage";
import 'cypress-file-upload';

describe("Content Types", () => {
    const homePage = new HomePage();
    const controlCenter = new ControlCenter();

    before(() => {
        if (Boolean = true) {
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            credentital.Email
            credentital.Password
            credentital.SignIn
        } else {
            // Okta Credentital Login page 
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            oktacredentital.LoginOrganization
            oktacredentital.UserName
            oktacredentital.Password
            oktacredentital.SignIn
        }
    });

    it('RD20-392,393,394: Verify Add, Modify and Delete Content Types', () => {
        expect(cy)
            .property('xpath')
            .to.be.a('function')
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false;
        });
        /*RD20-947 Add Content Types*/
        cy.wait(5000)
        cy.get(controlCenter.navigateControlCenterOptions).contains('Administration', { timeout: 3000 }).click({ force: true }).then(() => {
            cy.get('img[alt="Settings"]').click({ force: true }).then(() => {
                cy.wait(5000)
                cy.xpath('//span[contains(text(),"Content Settings")]').click().then(() => {
                    cy.wait(5000)
                    cy.get(':nth-child(2) > .MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label').click().then(() => {
                        cy.wait(5000)
                        cy.get('input[name="name"]').clear().type('Review Test')
                        cy.get('div[id="mui-component-select-reviewCycle"]').click().then(() => {
                            cy.xpath('//body/div[@id="menu-reviewCycle"]').contains('Annual').click()
                        })
                        cy.get('img[alt="Delete"]').click().then(() => {
                            cy.wait(2000)
                            cy.get('.MuiGrid-root > .MuiButtonBase-root > .MuiIconButton-label').click().then(() => {
                                cy.get('input[name="workflowStates.0.name"]').clear().type('Initiate')
                                cy.get('div[id="mui-component-select-workflowStates.0.group"]').click().then(() => {
                                    cy.xpath('//body/div[@id="menu-workflowStates.0.group"]').contains('Admin').click()
                                })
                                cy.get('input[name="workflowStates.0.canPublish"]').click()
                            })

                            cy.wait(2000)
                            cy.get('.MuiGrid-root > .MuiButtonBase-root > .MuiIconButton-label').click().then(() => {
                                cy.get('input[name="workflowStates.1.name"]').clear().type('Evaluate')
                                cy.get('div[id="mui-component-select-workflowStates.1.group"]').click().then(() => {
                                    cy.xpath('//body/div[@id="menu-workflowStates.1.group"]').contains('Admin').click()
                                })
                                cy.get('input[name="workflowStates.1.canPublish"]').click()
                            })

                            cy.wait(2000)
                            cy.get('.MuiGrid-root > .MuiButtonBase-root > .MuiIconButton-label').click().then(() => {
                                cy.get('input[name="workflowStates.2.name"]').clear().type('Edit')
                                cy.get('div[id="mui-component-select-workflowStates.2.group"]').click().then(() => {
                                    cy.xpath('//body/div[@id="menu-workflowStates.2.group"]').contains('Admin').click()
                                })
                                cy.get('input[name="workflowStates.2.canPublish"]').click()
                            })

                            cy.wait(2000)
                            cy.get('.MuiGrid-root > .MuiButtonBase-root > .MuiIconButton-label').click().then(() => {
                                cy.get('input[name="workflowStates.3.name"]').clear().type('Approval')
                                cy.get('div[id="mui-component-select-workflowStates.3.group"]').click().then(() => {
                                    cy.xpath('//body/div[@id="menu-workflowStates.3.group"]').contains('Admin').click()
                                })
                                cy.get('input[name="workflowStates.3.canPublish"]').click()
                            })

                            cy.wait(2000)
                            cy.get('.MuiGrid-root > .MuiButtonBase-root > .MuiIconButton-label').click().then(() => {
                                cy.get('input[name="workflowStates.4.name"]').clear().type('Publish')
                                cy.get('div[id="mui-component-select-workflowStates.4.group"]').click().then(() => {
                                    cy.xpath('//body/div[@id="menu-workflowStates.4.group"]').contains('Admin').click()
                                })
                                cy.get('input[name="workflowStates.4.canPublish"]').click()
                            })
                        })
                    })
                    cy.get('.MuiBox-root > form').contains('Save').click()
                })
                cy.wait(5000)
                cy.reload()
                /*RD20-948 Modify Content Types*/
                cy.xpath('//p[contains(text(),"Review Test")]').contains('Review Test').click().then(() => {
                    cy.wait(5000)
                    cy.get('input[name="name"]').clear().type('Document Test')
                        cy.get('div[id="mui-component-select-reviewCycle"]').click().then(() => {
                            cy.xpath('//body/div[@id="menu-reviewCycle"]').contains('Bi-annual').click()
                        })
                    cy.get('.Mui-selected').contains('Initiate').click().then(() => {
                        cy.get('input[name="workflowStates.0.name"]').clear().type('InitiateDoc')
                        cy.get('div[id="mui-component-select-workflowStates.0.group"]').click().then(() => {
                            cy.xpath('//body/div[@id="menu-workflowStates.0.group"]').contains('QA').click()
                        })
                        cy.get('input[name="workflowStates.0.canPublish"]').click()
                        cy.get('.MuiBox-root > form').contains('Save').click()
                        cy.wait(5000)
                    })
                })

                cy.xpath('//p[contains(text(),"Document Test")]').contains('Document Test').click().then(() => {
                    cy.wait(5000)
                    cy.get('.Mui-selected').contains('Evaluate').click().then(() => {
                        cy.get('input[name="workflowStates.1.name"]').clear().type('EvaluateDoc')
                        cy.get('div[id="mui-component-select-workflowStates.1.group"]').click().then(() => {
                            cy.xpath('//body/div[@id="menu-workflowStates.1.group"]').contains('QA').click()
                        })
                        cy.get('input[name="workflowStates.1.canPublish"]').click()
                        cy.get('.MuiBox-root > form').contains('Save').click()
                        cy.wait(5000)
                    })
                })

                cy.xpath('//p[contains(text(),"Document Test")]').contains('Document Test').click().then(() => {
                    cy.wait(5000)
                    cy.get('.Mui-selected').contains('Edit').click().then(() => {
                        cy.get('input[name="workflowStates.2.name"]').clear().type('EditDoc')
                        cy.get('div[id="mui-component-select-workflowStates.2.group"]').click().then(() => {
                            cy.xpath('//body/div[@id="menu-workflowStates.2.group"]').contains('QA').click()
                        })
                        cy.get('input[name="workflowStates.2.canPublish"]').click()
                        cy.get('.MuiBox-root > form').contains('Save').click()
                        cy.wait(5000)
                    })
                })

                cy.xpath('//p[contains(text(),"Document Test")]').contains('Document Test').click().then(() => {
                    cy.wait(5000)
                    cy.get('.Mui-selected').contains('Approval').click().then(() => {
                        cy.get('input[name="workflowStates.3.name"]').clear().type('ApprovalDoc')
                        cy.get('div[id="mui-component-select-workflowStates.3.group"]').click().then(() => {
                            cy.xpath('//body/div[@id="menu-workflowStates.3.group"]').contains('QA').click()
                        })
                        cy.get('input[name="workflowStates.3.canPublish"]').click()
                        cy.get('.MuiBox-root > form').contains('Save').click()
                        cy.wait(5000)
                    })
                })

                cy.xpath('//p[contains(text(),"Document Test")]').contains('Document Test').click().then(() => {
                    cy.wait(5000)
                    cy.get('.Mui-selected').contains('Publish').click().then(() => {
                        cy.get('input[name="workflowStates.4.name"]').clear().type('PublishDoc')
                        cy.get('div[id="mui-component-select-workflowStates.4.group"]').click().then(() => {
                            cy.xpath('//body/div[@id="menu-workflowStates.4.group"]').contains('QA').click()
                        })
                        cy.get('input[name="workflowStates.4.canPublish"]').click()
                        cy.get('.MuiBox-root > form').contains('Save').click()
                        cy.wait(5000)
                    })
                })
                cy.wait(5000)
                cy.reload()
                /*RD20-949 Delete Content Types*/
                cy.xpath('//p[contains(text(),"Document Test")]').contains('Document Test').click().then(() => {
                    cy.wait(5000)
                    cy.get('button[type="button"]').contains('Delete Type').click().then(() => {
                        cy.get(':nth-child(7) > .MuiDialog-container > .MuiPaper-root > .MuiDialogActions-root').contains('Yes').dblclick()
                    })
                })
                cy.wait(5000)
                cy.reload()
                /*Verify New Content Types no longer exist on Content Types*/
                cy.contains('Document Test').should('not.exist');
            })
        })
    })
})

