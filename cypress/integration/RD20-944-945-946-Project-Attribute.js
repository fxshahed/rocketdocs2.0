import { HomePage } from "../page-objects/HomePage";
import credentital from "../page-objects/credentital";
import ControlCenter from "../page-objects/PaneObject/controlCenter";
import oktacredentital from "../page-objects/oktaLoginPage";
import 'cypress-file-upload';

describe("Project Attributes", () => {
    const homePage = new HomePage();
    const controlCenter = new ControlCenter();

    before(() => {
        if (Boolean = true) {
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            credentital.Email
            credentital.Password
            credentital.SignIn
        } else {
            // Okta Credentital Login page 
            cy.visit("/")
            cy.visit(homePage.navigateToRocketdocsPage())
            oktacredentital.LoginOrganization
            oktacredentital.UserName
            oktacredentital.Password
            oktacredentital.SignIn
        }
    });

    it('RD20-944,945,946: Verify Add, Modify and Delete Project Attribute', () => {
        expect(cy)
            .property('xpath')
            .to.be.a('function')
        /*RD20-944 Add Project Attribute*/
        cy.wait(5000)
        cy.get(controlCenter.navigateControlCenterOptions).contains('Administration', { timeout: 3000 }).click({ force: true }).then(() => {
            cy.get('img[alt="Settings"]').click({ force: true }).then(() => {
                cy.get('img[alt="Project Settings"]').click({ timeout: 3000 }).then(() => {
                    cy.get('.MuiGrid-grid-sm-7 > .MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label').click()
                    cy.wait(2000)
                    cy.get('input[name="name"]').clear().type('Test2')
                    cy.get('input[name="showInList"]').click()
                    cy.get('input[name="searchFacet"]').click()
                    cy.get('div[id="mui-component-select-type"]').click().then(() => {
                        cy.xpath('//body/div[@id="menu-type"]').contains('Contact').click({ force: true })
                        cy.get('div[id="mui-component-select-groupId"]').click().then(() => {
                            cy.xpath('//body/div[@id="menu-groupId"]').contains('Admin').click()
                        })
                    })
                    cy.get('.MuiBox-root > form').contains('Save').click()
                    cy.reload()
                })
                /*RD20-945 Modify Project Attribute*/
                cy.wait(5000)
                cy.get('td[class="MuiTableCell-root MuiTableCell-body"]').contains('Test2').click().then(() => {
                    cy.wait(2000)
                    cy.get('input[name="name"]').clear().type('Test3')
                    cy.get('div[id="mui-component-select-groupId"]').click().then(() => {
                        cy.xpath('//body/div[@id="menu-groupId"]').contains('QA').click()
                    })
                    cy.get('.MuiBox-root > form').contains('Save').click()
                    cy.reload()
                    cy.wait(5000)
                })

                /*RD20-946 Delete Project Attribute*/
                cy.get('td[class="MuiTableCell-root MuiTableCell-body"]').contains('Test3').click().then(() => {
                    cy.wait(2000)
                    cy.get('.MuiBox-root > form').contains('Delete Attribute').click().then(() => {
                        cy.wait(3000)
                        cy.get(':nth-child(19) > .MuiDialog-container > .MuiPaper-root > .MuiDialogContent-root > .MuiBox-root').should('have.text', 'Are you sure you want to delete this Project Attribute?')
                        cy.get(':nth-child(19) > .MuiDialog-container > .MuiPaper-root > .MuiDialogActions-root >').contains('Yes').click()
                        cy.wait(3000)
                    })
                })
                cy.reload()
                /*Verify New Attribute no longer exist on Project Attribute*/
                cy.contains('Test3').should('not.exist');
            })
        })
    })
})
